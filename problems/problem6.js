const data = require("../js_drill_2.cjs");

function problem6() {
  const groupedByCountry = data.reduce((acc, employee) => {
    if (!acc[employee.location]) {
      acc[employee.location] = { totalSalary: 0, employeeCount: 0 };
    }
    acc[employee.location].totalSalary += parseFloat(employee.salary.replace("$", ""));
    acc[employee.location].employeeCount++;
    return acc;
  }, {});

  const averageSalaryByCountry = {};
  for (const country in groupedByCountry) {
    const { totalSalary, employeeCount } = groupedByCountry[country];
    averageSalaryByCountry[country] = totalSalary / employeeCount;
  }

  console.log(averageSalaryByCountry);
}
module.exports = problem6;