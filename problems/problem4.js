const data = require("./problem3.js");

function problem4() {
  const sumOfSalaries =
    data().reduce(function (sum, curr) {
      sum += curr.salary;
      return sum;
    }, 0) / 10000;
  console.log(sumOfSalaries);
}
module.exports = problem4;