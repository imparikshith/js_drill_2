const data = require("../js_drill_2.cjs");

function problem1() {
  const webDevelopers = data.filter((x) => x.job.startsWith("Web Developer"));
  console.log(webDevelopers);
}
module.exports = problem1;