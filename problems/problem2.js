const data = require("../js_drill_2.cjs");

function problem2() {
  const newData = data.map(function (x) {
    x.salary = parseFloat(x.salary.replace("$", ""));
    return x;
  });
  return newData;
}
module.exports = problem2;