const data = require("./problem2.js");

function problem3() {
  const updatedSalary = data().map(function (x) {
    x.salary *= 10000;
    return x;
  });
  return updatedSalary;
}
module.exports = problem3;