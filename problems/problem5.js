const data = require("./problem3.js");

function problem5() {
  const salariesByCountry = data().reduce(function (acc, curr) {
    if (acc[curr.location]) {
      acc[curr.location] = (acc[curr.location] * 10000 + curr.salary) / 10000;
    } else {
      acc[curr.location] = curr.salary / 10000;
    }
    return acc;
  }, {});
  return salariesByCountry;
}
module.exports = problem5;